import svg4everybody from 'svg4everybody';
import Bgview from './components/bgview';
import './components/dropdown';

const App = (() => {
  return {
    init() {
      svg4everybody();
      $('.dropdown').dropdown();
      new Bgview();
    }
  }
})();

App.init();
