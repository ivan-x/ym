export default class Bgview {

  constructor() {
    this.$body = $(document.body);
    this.$bgitems = $('.js-bgview-item');
    this.$plans = $('.plan');
    this.$currentPlan = null;
    this.$currentView = null;
    this.timer = null;
    this.hover = false;

    this.events();
  }

  events() {
    this.$plans
      .on('mouseenter', $.proxy(this.enter, this))
      .on('mouseleave touchend', $.proxy(this.leave, this));
  }

  appendVideo() {

    const wrap = document.createElement('div');
    wrap.id = 'wrapvideo';
    wrap.className = 'wrap-video';
    const video = document.createElement('video');
    const source = document.createElement('source');

    video.className = 'video-screen';
    video.id = 'bgvideo';
    video.controls = false;
    video.muted = true;
    video.loop = true;
    video.autoplay = true;

    source.type = 'video/mp4';
    source.src = '../img/anni_90.mp4';
    // source.setAttribute('data-src', src);
    video.appendChild(source);
    wrap.appendChild(video);
    this.$currentView.append(wrap);

    this.video = video;

    setTimeout(() => {
      video.className += ' is-show';
    }, 0);
  }

  enter(e) {
    if (this.hover) return;

    this.$body.addClass('hover-plan');
    this.$currentPlan = $(e.target).closest('.plan');
    this.$currentView = this.$bgitems.filter(`#${this.$currentPlan.data('bgview-id')}`);
    this.$currentPlan.addClass('is-active');

    this.$currentView
      .addClass('fade')
      .siblings()
      .removeClass('fade');

    if (this.timer) clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.appendVideo();
      this.$body.addClass('hover-video');
    }, 2000);

    this.hover = true;

  }

  leave(e) {
    if (!this.hover) return;

    this.$body.removeClass('hover-plan hover-video');

    this.$currentPlan.removeClass('is-active');
    this.$currentView.removeClass('fade');

    clearTimeout(this.timer);

    if (this.video) {
      $(this.video).remove();
    }

    $('#wrapvideo').remove();

    this.$currentPlan = this.$currentView = null;
    this.video = null;
    this.hover = false;
  }
}
