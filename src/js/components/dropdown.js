$.fn.dropdown = function() {
  return this.each(function() {
    const $dd = $(this);
    const $placeholder = $dd.find('.dropdown__placeholder');
    const $options = $dd.find('.dropdown__item');

    const htmlValue = $options.filter('.has-selected').html();
    $placeholder.html(htmlValue);

    function toggle(evt) {
      evt.preventDefault();
      $dd.toggleClass('is-open');
    }

    function select(evt) {
      evt.preventDefault();
      const $option = $(this);
      const htmlValue = $option.html();
      $placeholder.html(htmlValue);
    }

    $dd.on('click', toggle);
    $options.on('click', select);
    $(document).on('click', function(e) {
      const isDropdown = $.contains($dd[0], e.target) || $dd[0] === e.target;
      if (!isDropdown && $dd.hasClass('is-open')) {
        $dd.removeClass('is-open');
      }
    });
  });
};
